from pyelf import *

class PyELF64_x86(PyELF):
    def __init__(self):
        PyELF.__init__(self)
        self.ei_class = bytearray(b'\x02') # Elf32 (Default, x86)
        self.ei_data = bytearray(b'\x01') # Little End (Default, x86)
        self.e_ident()
        self.e_mach = bytearray(b'\x03\x00') #dw
        self.ehdr()

if __name__ == '__main__':
    mybin = PyELF64_x86()
    hellobin = mybin.exe(mybin.hello())
    outpath = 'hello'
    myfile = open(outpath,'wb')
    myfile.write(hellobin)
    myfile.close()
