
def i_to_b(someaddr, bytes):
    hexstr = hex(someaddr)
    hexstr = hexstr[0:2]+(bytes-(len(hex(someaddr))-2))%8*"0"+hexstr[2:]
    #print hexstr
    hexarray = [ord(c) for c in hexstr[2:].decode('hex')]
    hexarray.reverse()
    return bytearray(hexarray)


class PyELF():
    'Class to define valid, Linux (ELF) Executable and Linkable Format Binaries'
    def __init__(self):
        # These magic numbers say this is an ELF binary
        self.ei_magic = bytearray(b'\x7F\x45\x4C\x46')
        self.ei_class = bytearray(b'\x00') # Invalid
        self.ei_data = bytearray(b'\x00') # Endianness undefined
        self.ei_ver = bytearray(b'\x01') # Only 1 elf spec so far
        self.e_ident()
        self.e_pad8 = bytearray(b'\x00\x00\x00\x00\x00\x00\x00\x00\x00')
        self.e_type = bytearray(b'\x02\x00') #dw
        self.e_mach = bytearray(b'\x00\x00') #dw
        self.e_ver = bytearray(b'\x01\x00\x00\x00') #dd

        self.baseaddr = 0x08000000 #<min;Max is 0xffffffff
        self.e_entry = i_to_b(self.baseaddr+int(0x54),8)
        # Relative offset of the p[rogram] header
        self.e_phoff = bytearray(b'\x34\x00\x00\x00') #dd
        #e_phoff = i_to_b(0x34000000,8)
        # Relative header offset
        self.e_shoff = bytearray(b'\x00\x00\x00\x00') #dd
        self.e_flags = bytearray(b'\x00\x00\x00\x00') #dd 
        self.e_phnum = bytearray(b'\x01\x00') #dw
        self.e_shentsize = bytearray(b'\x00\x00') #dw
        self.e_shnum = bytearray(b'\x00\x00') #dw
        self.e_shstrndx = bytearray(b'\x00\x00') #dw
        self.e_ehdrsize() # = bytearray(b'\x00\x00') #dw

        self.p_vaddr = i_to_b(self.baseaddr,8)
        filesize = 0x0000006F
        self.p_filesz = i_to_b(filesize,8)
        self.p_memsz = i_to_b(filesize,8)
        self.p_type = bytearray(b'\x01\x00\x00\x00') #dd
        self.p_offset = bytearray(b'\x00\x00\x00\x00') #dd
        self.p_paddr = i_to_b(self.baseaddr,8) #dd
        self.p_flags = bytearray(b'\x05\x00\x00\x00') #dd
        self.p_align = bytearray(b'\x00\x10\x00\x00') #dd
        self.e_phdrsize()
        self.e_ehdrsize()
        self.ehdr()
        
        self._binary = self.hello()
        self._strtab = self.strtab()
        filesize = len(self._binary)+len(self.ehdr())+len(self.phdr())
        self.p_filesz = i_to_b(filesize-1,8)
        self.p_memsz = i_to_b(filesize-1,8)
        self.phdr()


    def exe(self,binary=None):
        if binary == None:
            binary = self._binary
        else:
            self._binary = binary
            filesize = len(self._binary)+len(self.ehdr())+len(self.phdr())
            self.p_filesz = i_to_b(filesize-1,8)
            self.p_memsz = i_to_b(filesize-1,8)
            self.phdr()
        return self.ehdr() + self.phdr() + self._binary + self._strtab


    def ehdr(self):
        'ELF header.  Every valid ELF binary has an ehdr.'
        myehdr = self.e_ident() \
        + self.e_pad8 \
        + self.e_type \
        + self.e_mach \
        + self.e_ver \
        + self.e_entry \
        + self.e_phoff \
        + self.e_shoff \
        + self.e_flags \
        + self.e_ehdrsize() \
        + self.e_phdrsize() \
        + self.e_phnum \
        + self.e_shentsize \
        + self.e_shnum \
        + self.e_shstrndx
        return myehdr


    def phdr(self):
        'Program header.  Every valid ELF binary has a phdr.'
        myphdr = self.p_type \
        + self.p_offset \
        + self.p_vaddr \
        + self.p_paddr \
        + self.p_filesz \
        + self.p_memsz \
        + self.p_flags \
        + self.p_align
        return myphdr


    def add_shdr(self,):


    def e_ehdrsize(self):
        esize = self.e_ident() \
        + self.e_pad8 \
        + self.e_type \
        + self.e_mach \
        + self.e_ver \
        + self.e_entry \
        + self.e_phoff \
        + self.e_shoff \
        + self.e_flags \
        + i_to_b(0x0000,4) \
        + i_to_b(0x0000,4) \
        + self.e_phnum \
        + self.e_shentsize \
        + self.e_shnum \
        + self.e_shstrndx
        return i_to_b(len(esize),4)


    def e_phdrsize(self):
        psize = self.p_type \
        + self.p_offset \
        + self.p_vaddr \
        + self.p_paddr \
        + self.p_filesz \
        + self.p_memsz \
        + self.p_flags \
        + self.p_align
        return i_to_b(len(psize),4)


    def e_ident(self):
        ident = self.ei_magic \
        + self.ei_class \
        + self.ei_data \
        + self.ei_ver
        return ident

    def strtab(self,strings=None):
        'pass in a list of strings'
        self.sh_size = 0
        self._strtab = ''
        if strings == None:
            return bytearray()
        else: 
            for each in strings:
                self.sh_size += len(each)+1
                self._strtab += each + '\0'
        
        return self._strtab

    def hello(self):
        from opcodes import movAL,movDL,movECX,int80,br
        self.baseaddr
        msg = bytearray(b'Hello World!', 'ascii')
        msglen = bytearray([len(msg)+1])# \x0C = 12
        msgaddr = i_to_b(self.baseaddr+0x63,8)
        self._binary = bytearray()
        
        self._binary += movAL + bytearray([4]) # syscall print a string
        self._binary += movDL + msglen # String Length
        self._binary += movECX + msgaddr # Pointer to start of string 
        self._binary += int80 # Go
        
        self._binary += movAL + bytearray([1]) # syscall to exit
        self._binary += int80 # Go
        
        self._binary += msg + br
        return self._binary
