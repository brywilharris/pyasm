;nasm -f bin -o nasmhello play.asm; hexdump nasmhello
;nasm -f elf64 -o nasmhello play.asm; hexdump nasmhello
;nasm -f elf64 play.asm; ld -s -o play play.o; ./play > output; echo "play:"; hexdump -C play; echo "output:"; hexdump -C output; diff play output
; This program (currently) dumps its own memory when run, so that, if piped into an output file, it makes an exact copy of itself.  The idea is that this method could be used to deug assembly programs without a separate graphical debugger.

section .text
    global _start
_start:    
    mov eax,4
    mov ebx,1
    ;mov ecx,$$-0xb0 ; How to figure this out programatically?
    mov ecx,msg
    mov edx,len
    int 0x80
    
    mov ebx,0
    mov eax,1
    int 0x80
    ;padstart:
    ;pad equ 0x01000 - $ - 0x1d8 ; How to figure this out programatically?
    ;times pad db 0x90
    ;middle equ padstart + 0x10
    ;mov 0x100,0xa
    
section .data
    msg db 'Hello World!',0xa
    len equ $ - msg
  

