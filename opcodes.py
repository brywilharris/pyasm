#Documentation found at http://css.csail.mit.edu/6.858/2013/readings/i386/toc.htm

# Register Add bytecodes
addr8torm = bytearray(b'\x00') # ADD r/m8,r8 2/7 Add byte register to r/m byte
addr32torm = bytearray(b'\x01') # ADD r/m32,r32 2/7 Add dword register to r/m dword
addrmtor8 = bytearray(b'\x02') # ADD r32,r/m32 2/6 Add r/m dword to dword register
addrmtor32 = bytearray(b'\x03') # ADD r32,r/m32 2/6 Add r/m dword to dword register
addAL = bytearray(b'\x04') # ADD AL,imm8 2 Add immediate byte to AL
addEAX = bytearray(b'\x05') # ADD EAX,imm32 2 Add immediate dword to EAX

pushES = bytearray(b'\x06') # Pushes ES onto the stack
popES = bytearray(b'\x07') # Pops from the stack into ES 

orr8torm = bytearray(b'\x08') # 08 /r OR r/m8,r8 2/6 OR byte register to r/m byte
orr32torm = bytearray(b'\x09') # 09 /r OR r/m16,r16 2/6 OR word register to r/m word
# 09 /r OR r/m32,r32 2/6 OR dword register to r/m dword
addrmtor8 = bytearray(b'\x0a') # 0A /r OR r8,r/m8 2/7 OR byte register to r/m byte
addrmtor32 = bytearray(b'\x0b') # 0B /r OR r16,r/m16 2/7 OR word register to r/m word
# 0B /r OR r32,r/m32 2/7 OR dword register to r/m dword

orAL = bytearray(b'\x0c')
orEAX = bytearray(b'\x0d')

pushCS = bytearray(b'\x0e')
# b'\0f' is an opcode which means "two bytes" or "a two byte opcode follows"

#Add with carry instructions
adcr8torm = bytearray(b'\x10') # 10 /r ADC r/m8,r8 2/7 Add with carry byte register to r/m byte
adcr32torm = bytearray(b'\x11') # 11 /r ADC r/m16,r16 2/7 Add with carry word register to r/m word
# 11 /r ADC r/m32,r32 2/7 Add with CF dword register to r/m dword
adcrmtor8 = bytearray(b'\x12') # 12 /r ADC r8,r/m8 2/6 Add with carry r/m byte to byte register
adcrmtor32 = bytearray(b'\x13') # 13 /r ADC r16,r/m16 2/6 Add with carry r/m word to word register
# 13 /r ADC r32,r/m32 2/6 Add with CF r/m dword to dword register
adcAL = bytearray(b'\x14') # 14 ib ADC AL,imm8 2 Add with carry immediate byte to AL
adcEAX = bytearray(b'\x15') # 15 iw ADC AX,imm16 2 Add with carry immediate word to AX
# 15 id ADC EAX,imm32 2 Add with carry immediate dword to EAX

pushSS = bytearray(b'\x16') # Pushes SS onto the stack
popSS = bytearray(b'\x17') # Pops from the stack into SS 

#Subtract with borrow instructions
sbbr8torm = bytearray(b'\x18')
sbbr32torm = bytearray(b'\x19') 
sbbrmtor8 = bytearray(b'\x1a')
sbbrmtor32 = bytearray(b'\x1b')
sbbAL = bytearray(b'\x1c')
sbbEAX = bytearray(b'\x1d')

pushDS = bytearray(b'\x1e')
popDS = bytearray(b'\x1f')

#And instructions (Binary And)
andr8torm = bytearray(b'\x20')
andr32torm = bytearray(b'\x21') 
andrmtor8 = bytearray(b'\x22')
andrmtor32 = bytearray(b'\x23')
andAL = bytearray(b'\x24')
andEAX = bytearray(b'\x25')
# 20 /r AND r/m8,r8 2/7 AND byte register to r/m byte
# 21 /r AND r/m16,r16 2/7 AND word register to r/m word
# 21 /r AND r/m32,r32 2/7 AND dword register to r/m dword
# 22 /r AND r8,r/m8 2/6 AND r/m byte to byte register
# 23 /r AND r16,r/m16 2/6 AND r/m word to word register
# 23 /r AND r32,r/m32 2/6 AND r/m dword to dword register
# 24 ib AND AL,imm8 2 AND immediate byte to AL
# 25 iw AND AX,imm16 2 AND immediate word to AX
# 25 id AND EAX,imm32 2 AND immediate dword to EAX

_ES = bytearray(b'\x26')
daa = bytearray(b'\x27') # Decimal adjust AL after addition (BCD math)
# 27 DAA 4 Decimal adjust AL after addition

#Subtract 
subr8torm = bytearray(b'\x28')
subr32torm = bytearray(b'\x29') 
subrmtor8 = bytearray(b'\x2a')
subrmtor32 = bytearray(b'\x2b')
subAL = bytearray(b'\x2c')
subEAX = bytearray(b'\x2d')

_CS = bytearray(b'\x2e')
das = bytearray(b'\x2f') # Decimal adjust AL after subtraction (BCD math)
# 2F DAS 4 Decimal adjust AL after subtraction

#And instructions (Binary And)
xorr8torm = bytearray(b'\x30')
xorr32torm = bytearray(b'\x31') 
xorrmtor8 = bytearray(b'\x32')
xorrmtor32 = bytearray(b'\x33')
xorAL = bytearray(b'\x34')
xorEAX = bytearray(b'\x35')

_SS = bytearray(b'\x36')
aaa = bytearray(b'\x37') # ASCII adjust AL after addition (BCD math)
# 37 AAA 4 ASCII adjust AL after addition

#Compare
cmpr8torm = bytearray(b'\x38')
cmpr32torm = bytearray(b'\x39') 
cmprmtor8 = bytearray(b'\x3a')
cmprmtor32 = bytearray(b'\x3b')
cmpAL = bytearray(b'\x3c')
cmpEAX = bytearray(b'\x3d')

# 38 /r CMP r/m8,r8 2/5 Compare byte register to r/m byte
# 39 /r CMP r/m16,r16 2/5 Compare word register to r/m word
# 39 /r CMP r/m32,r32 2/5 Compare dword register to r/m dword
# 3A /r CMP r8,r/m8 2/6 Compare r/m byte to byte register
# 3B /r CMP r16,r/m16 2/6 Compare r/m word to word register
# 3B /r CMP r32,r/m32 2/6 Compare r/m dword to dword register
# 3C ib CMP AL,imm8 2 Compare immediate byte to AL
# 3D iw CMP AX,imm16 2 Compare immediate word to AX
# 3D id CMP EAX,imm32 2 Compare immediate dword to EAX


_DS = bytearray(b'\x3e')

aas = bytearray(b'\x3f') # ASCII adjust AL after subtraction (BCD math)
# 3F AAS 4 ASCII adjust AL after subtraction

#Increment registers
incEAX = bytearray(b'\x40')
incECX = bytearray(b'\x41')
incEDX = bytearray(b'\x42')
incEBX = bytearray(b'\x43')
incESP = bytearray(b'\x44')
incEBP = bytearray(b'\x45')
incESI = bytearray(b'\x46')
incEDI = bytearray(b'\x47')

#Decrement registers
decEAX = bytearray(b'\x48')
decECX = bytearray(b'\x49')
decEDX = bytearray(b'\x4a')
decEBX = bytearray(b'\x4b')
decESP = bytearray(b'\x4c')
decEBP = bytearray(b'\x4d')
decESI = bytearray(b'\x4e')
decEDI = bytearray(b'\x4f')
# 48+rw DEC r16 2 Decrement word register by 1
# 48+rw DEC r32 2 Decrement dword register by 1

#Push registers
pushEAX = bytearray(b'\x50')
pushECX = bytearray(b'\x51')
pushEDX = bytearray(b'\x52')
pushEBX = bytearray(b'\x53')
pushESP = bytearray(b'\x54')
pushEBP = bytearray(b'\x55')
pushESI = bytearray(b'\x56')
pushEDI = bytearray(b'\x57')

#Pop registers
popEAX = bytearray(b'\x58')
popECX = bytearray(b'\x59')
popEDX = bytearray(b'\x5a')
popEBX = bytearray(b'\x5b')
popESP = bytearray(b'\x5c')
popEBP = bytearray(b'\x5d')
popESI = bytearray(b'\x5e')
popEDI = bytearray(b'\x5f')

pushA = bytearray(b'\x60') #push all GP registers onto the stack
# 60 PUSHA 18 Push AX, CX, DX, BX, original SP, BP, SI, and DI
# 60 PUSHAD 18 Push EAX, ECX, EDX, EBX, original ESP, EBP, ESI, and EDI

popA = bytearray(b'\x61') #pop all GP registers from the stack
#61 POPA 24 Pop DI, SI, BP, SP, BX, DX, CX, and AX
#61 POPAD 24 Pop EDI, ESI, EBP, ESP, EDX, ECX, and EAX

bound = bytearray(b'\x62') #Check if r16 or r32 is in bounds 
# BOUND r16,m16&16 10 Check if r16 is within bounds 62 /r 
# BOUND r32,m32&32 10 Check if r32 is within bounds

# 63 /r ARPL r/m16,r16 pm=20/21 Adjust RPL of r/m16 to not less than RPL of r16

# 69 /r iw IMUL r16,r/m16,imm16 9-22/12-25 word register := r/m16 * immediate word
# 69 /r id IMUL r32,r/m32,imm32 9-38/12-41 dword register := r/m32 * immediate dword
# 69 /r iw IMUL r16,imm16 9-22/12-25 word register := r/m16 * immediate word
# 69 /r id IMUL r32,imm32 9-38/12-41 dword register := r/m32 * immediate dword
# 6B /r ib IMUL r16,r/m16,imm8 9-14/12-17 word register := r/m16 * sign-extended immediate byte
# 6B /r ib IMUL r32,r/m32,imm8 9-14/12-17 dword register := r/m32 * sign-extended immediate byte
# 6B /r ib IMUL r16,imm8 9-14/12-17 word register := word register * sign-extended immediate byte
# 6B /r ib IMUL r32,imm8 9-14/12-17 dword register := dword register * sign-extended immediate byte

addi8 = bytearray(b'\x80') # ADD r/m8,imm8 2/7 Add immediate byte to r/m byte
addi16 = bytearray(b'\x81') # ADD r/m16,imm16 2/7 Add immediate word to r/m word
addi32 = bytearray(b'\x81') # ADD r/m32,imm32 2/7 Add immediate dword to r/m dword
addis16 = bytearray(b'\x83') # ADD r/m16,imm8 2/7 Add sign-extended immediate byte to r/m word
addis32 = bytearray(b'\x83') # ADD r/m32,imm8 2/7 Add sign-extended immediate byte to r/m dword
# 80 /0 ib ADD r/m8,imm8 2/7 Add immediate byte to r/m byte
# 81 /0 iw ADD r/m16,imm16 2/7 Add immediate word to r/m word
# 81 /0 id ADD r/m32,imm32 2/7 Add immediate dword to r/m dword
# 83 /0 ib ADD r/m16,imm8 2/7 Add sign-extended immediate byte to r/m word
# 83 /0 ib ADD r/m32,imm8 2/7 Add sign-extended immediate byte to r/m dword
# 80 /2 ib ADC r/m8,imm8 2/7 Add with carry immediate byte to r/m byte
# 81 /2 iw ADC r/m16,imm16 2/7 Add with carry immediate word to r/m word
# 81 /2 id ADC r/m32,imm32 2/7 Add with CF immediate dword to r/m dword
# 83 /2 ib ADC r/m16,imm8 2/7 Add with CF sign-extended immediate byte to r/m word
# 83 /2 ib ADC r/m32,imm8 2/7 Add with CF sign-extended immediate byte into r/m dword
# 80 /4 ib AND r/m8,imm8 2/7 AND immediate byte to r/m byte
# 81 /4 iw AND r/m16,imm16 2/7 AND immediate word to r/m word
# 81 /4 id AND r/m32,imm32 2/7 AND immediate dword to r/m dword
# 83 /4 ib AND r/m16,imm8 2/7 AND sign-extended immediate byte with r/m word
# 83 /4 ib AND r/m32,imm8 2/7 AND sign-extended immediate byte with r/m dword
# 80 /7 ib CMP r/m8,imm8 2/5 Compare immediate byte to r/m byte
# 81 /7 iw CMP r/m16,imm16 2/5 Compare immediate word to r/m word
# 81 /7 id CMP r/m32,imm32 2/5 Compare immediate dword to r/m dword
# 83 /7 ib CMP r/m16,imm8 2/5 Compare sign extended immediate byte to r/m word
# 83 /7 ib CMP r/m32,imm8 2/5 Compare sign extended immediate byte to r/m dword

# 98 CBW 3 AX := sign-extend of AL
# 98 CWDE 3 EAX := sign-extend of AX
# 99 CWD 2 DX:AX := sign-extend of AX
# 99 CDQ 2 EDX:EAX := sign-extend of EAX

# MOVS/MOVSB/MOVSW/MOVSD -- Move Data from String to String
movsb = bytearray(b'\xA4') # A4 MOVS m8,m8 7 Move byte [(E)SI] to ES:[(E)DI]
movsw = bytearray(b'\xA5') # A5 MOVS m16,m16 7 Move word [(E)SI] to ES:[(E)DI]
movsd = bytearray(b'\xA5') # A5 MOVS m32,m32 7 Move dword [(E)SI] to ES:[(E)DI]
movdsb = bytearray(b'\xA4') # A4 MOVSB 7 Move byte DS:[(E)SI] to ES:[(E)DI]
movdsw = bytearray(b'\xA5') # A5 MOVSW 7 Move word DS:[(E)SI] to ES:[(E)DI]
movdsd = bytearray(b'\xA5') # A5 MOVSD 7 Move dword DS:[(E)SI] to ES:[(E)DI]

# A6 CMPS m8,m8 10 Compare bytes ES:[(E)DI] (second operand) with [(E)SI] (first operand)
# A7 CMPS m16,m16 10 Compare words ES:[(E)DI] (second operand) with [(E)SI] (first operand)
# A7 CMPS m32,m32 10 Compare dwords ES:[(E)DI] (second operand) with [(E)SI] (first operand)
# A6 CMPSB 10 Compare bytes ES:[(E)DI] with DS:[SI]
# A7 CMPSW 10 Compare words ES:[(E)DI] with DS:[SI]
# A7 CMPSD 10 Compare dwords ES:[(E)DI] with DS:[SI]

# Immediate Move bytecodes
movAL = bytearray(b'\xB0') # MOV reg8,imm8 2 Move immediate byte to register
movCL = bytearray(b'\xB1') #
movDL = bytearray(b'\xB2') #
movbL = bytearray(b'\xB3') #
movAH = bytearray(b'\xB4') #
movCH = bytearray(b'\xB5') #
movDH = bytearray(b'\xB6') #
movBH = bytearray(b'\xB7') #
movAX = bytearray(b'\xB8') # MOV reg16,imm16 2 Move immediate word to register
movCX = bytearray(b'\xB9') #
movDX = bytearray(b'\xBA') #
movBX = bytearray(b'\xBB') #
movSP = bytearray(b'\xBC') #
movBP = bytearray(b'\xBD') #
movSI = bytearray(b'\xBE') #
movDI = bytearray(b'\xBF') #
movEAX = bytearray(b'\xB8') # MOV reg32,imm32 2 Move immediate dword to register
movECX = bytearray(b'\xB9') #
movEDX = bytearray(b'\xBA') #
movEBX = bytearray(b'\xBB') #
movESP = bytearray(b'\xBC') #
movEBP = bytearray(b'\xBD') #
movESI = bytearray(b'\xBE') #
movEDI = bytearray(b'\xBF') #

# C8 iw 00 ENTER imm16,0 10 Make procedure stack frame
# C8 iw 01 ENTER imm16,1 12 Make stack frame for procedure parameters
# C8 iw ib ENTER imm16,imm8 15+4(n-1) Make stack frame for procedure parameters


# D4 0A AAM 17 ASCII adjust AX after multiply
# D5 0A AAD 19 ASCII adjust AX before division

# F4 HLT 5 Halt

#MUL -- Unsigned Multiplication of AL or AX
mulAL = bytearray(b'\xF6') # F6 /4 MUL AL,r/m8 9-14/12-17 Unsigned multiply (AX := AL * r/m byte)
mulAX = bytearray(b'\xF7') # F7 /4 MUL AX,r/m16 9-22/12-25 Unsigned multiply (DX:AX := AX * r/m word)
mulEAX = bytearray(b'\xF7') # F7 /4 MUL EAX,r/m32 9-38/12-41 Unsigned multiply (EDX:EAX := EAX * r/m dword)

# F5 CMC 2 Complement carry flag
# F6 /5 IMUL r/m8 9-14/12-17 AX= AL * r/m byte
# F7 /5 IMUL r/m16 9-22/12-25 DX:AX := AX * r/m word
# F7 /5 IMUL r/m32 9-38/12-41 EDX:EAX := EAX * r/m dword
# F6 /6 DIV AL,r/m8 14/17 Unsigned divide AX by r/m byte (AL=Quo, AH=Rem)
# F7 /6 DIV AX,r/m16 22/25 Unsigned divide DX:AX by r/m word (AX=Quo, DX=Rem)
# F7 /6 DIV EAX,r/m32 38/41 Unsigned divide EDX:EAX by r/m dword (EAX=Quo, EDX=Rem)
# F6 /7 IDIV r/m8 19 Signed divide AX by r/m byte (AL=Quo, AH=Rem)
# F7 /7 IDIV AX,r/m16 27 Signed divide DX:AX by EA word (AX=Quo, DX=Rem)
# F7 /7 IDIV EAX,r/m32 43 Signed divide EDX:EAX by DWORD byte (EAX=Quo, EDX=Rem)
# F8 CLC 2 Clear carry flag

# FA CLI 3 Clear interrupt flag; interrupts disabled
# FC CLD 2 Clear direction flag; SI and DI will increment during string instructions

# FE /1 DEC r/m8 2/6 Decrement r/m byte by 1
# FF /1 DEC r/m16 2/6 Decrement r/m word by 1
# FF /1 DEC r/m32 2/6 Decrement r/m dword by 1

# OF 06 CLTS 5 Clear task-switched flag

# Move to/from Special Registers Require piv level 0
movecrr = bytearray(b'\x0F\x20') # 0F 20 /r MOV r32,CR0/CR2/CR3 6 Move (control register) to (register)
movercr = bytearray(b'\x0F\x22') # 0F 22 /r MOV CR0/CR2/CR3,r32 10/4/5 Move (register) to (control register)
moverd0 = bytearray(b'\x0F\x21') # 0F 21 /r MOV r32,DR0 -- 3 22 Move (debug register) to (register)
moverd6 = bytearray(b'\x0F\x21') # 0F 21 /r MOV r32,DR6/DR7 14 Move (debug register) to (register)
moved0r = bytearray(b'\x0F\x23') # 0F 23 /r MOV DR0 -- 3,r32 22 Move (register) to (debug register)
moved6r = bytearray(b'\x0F\x23') # 0F 23 /r MOV DR6/DR7,r32 16 Move (register) to (debug register)
movetrr = bytearray(b'\x0F\x24') # 0F 24 /r MOV r32,TR6/TR7 12 Move (test register) to (register)
movertr = bytearray(b'\x0F\x26') # 0F 26 /r MOV TR6/TR7,r32 12 Move (register) to (test register)

# 0F A3 BT r/m16,r16 3/12 Save bit in carry flag
# 0F A3 BT r/m32,r32 3/12 Save bit in carry flag
# 0F AB BTS r/m16,r16 6/13 Save bit in carry flag and set
# 0F AB BTS r/m32,r32 6/13 Save bit in carry flag and set

# 0F AF /r IMUL r16,r/m16 9-22/12-25 word register := word register * r/m word
# 0F AF /r IMUL r32,r/m32 9-38/12-41 dword register := dword register * r/m dword

# 0F B3 BTR r/m16,r16 6/13 Save bit in carry flag and reset
# 0F B3 BTR r/m32,r32 6/13 Save bit in carry flag and reset
# 0F BA /4 ib BT r/m16,imm8 3/6 Save bit in carry flag
# 0F BA /4 ib BT r/m32,imm8 3/6 Save bit in carry flag
# 0F BA /5 ib BTS r/m16,imm8 6/8 Save bit in carry flag and set
# 0F BA /5 ib BTS r/m32,imm8 6/8 Save bit in carry flag and set
# 0F BA /6 ib BTR r/m16,imm8 6/8 Save bit in carry flag and reset
# 0F BA /6 ib BTR r/m32,imm8 6/8 Save bit in carry flag and reset
# 0F BA /7 ib BTC r/m16,imm8 6/8 Save bit in carry flag and complement
# 0F BA /7 ib BTC r/m32,imm8 6/8 Save bit in carry flag and complement

# 0F BB BTC r/m16,r16 6/13 Save bit in carry flag and complement
# 0F BB BTC r/m32,r32 6/13 Save bit in carry flag and complement

# 0F BC BSF r16,r/m16 10+3n Bit scan forward on r/m word
# 0F BC BSF r32,r/m32 10+3n Bit scan forward on r/m dword

# 0F BD BSR r16,r/m16 10+3n Bit scan reverse on r/m word
# 0F BD BSR r32,r/m32 10+3n Bit scan reverse on r/m dword

#MOVZX -- Move with Zero-Extend
movzxb = bytearray(b'\x0F\xB6') # 0F B6 /r MOVZX r16,r/m8 3/6 Move byte to word with zero-extend
movzxw = bytearray(b'\x0F\xB6') # 0F B6 /r MOVZX r32,r/m8 3/6 Move byte to dword, zero-extend
movzxd = bytearray(b'\x0F\xB7') # 0F B7 /r MOVZX r32,r/m16 3/6 Move word to dword, zero-extend

# MOVSX -- Move with Sign-Extend
movsxb = bytearray(b'\x66\x0F\xBE') # 0F BE /r MOVSX r16,r/m8 3/6 Move byte to word with sign-extend
movsxw = bytearray(b'\x0F\xBE') # 0F BE /r MOVSX r32,r/m8 3/6 Move byte to dword, sign-extend
movsxd = bytearray(b'\x0F\xBE') # 0F BF /r MOVSX r32,r/m16 3/6 Move word to dword, sign-extend

# Function Calls:
# 9A cd CALL ptr16:16 17+m,pm=34+m Call intersegment, to full pointer given
# 9A cd CALL ptr16:16 pm=52+m Call gate, same privilege
# 9A cd CALL ptr16:16 pm=86+m Call gate, more privilege, no parameters
# 9A cd CALL ptr16:16 pm=94+4x+m Call gate, more privilege, x parameters
# 9A cd CALL ptr16:16 ts Call to task
# 9A cp CALL ptr16:32 17+m,pm=34+m Call intersegment, to full pointer given
# 9A cp CALL ptr16:32 pm=52+m Call gate, same privilege
# 9A cp CALL ptr16:32 pm=86+m Call gate, more privilege, no parameters
# 9A cp CALL ptr32:32 pm=94+4x+m Call gate, more privilege, x parameters
# 9A cp CALL ptr16:32 ts Call to task
# E8 cw CALL rel16 7+m Call near, displacement relative to next instruction
# E8 cd CALL rel32 7+m Call near, displacement relative to next instruction
# FF /2 CALL r/m16 7+m/10+m Call near, register indirect/memory indirect
# FF /3 CALL m16:16 22+m,pm=38+m Call intersegment, address at r/m dword
# FF /3 CALL m16:16 pm=56+m Call gate, same privilege
# FF /3 CALL m16:16 pm=90+m Call gate, more privilege, no parameters
# FF /3 CALL m16:16 pm=98+4x+m Call gate, more privilege, x parameters
# FF /3 CALL m16:16 5 + ts Call to task
# FF /2 CALL r/m32 7+m/10+m Call near, indirect
# FF /3 CALL m16:32 22+m,pm=38+m Call intersegment, address at r/m dword
# FF /3 CALL m16:32 pm=56+m Call gate, same privilege
# FF /3 CALL m16:32 pm=90+m Call gate, more privilege, no parameters
# FF /3 CALL m16:32 pm=98+4x+m Call gate, more privilege, x parameters
# FF /3 CALL m16:32 5 + ts Call to task

#Linux syscall (Int80)
int80 = bytearray(b'\xCD\x80')

# Other constant definitions
# Newline 
br = bytearray(b'\x0A')

