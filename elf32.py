from pyelf import *
from opcodes import *

class PyELF32_x86(PyELF):
    def __init__(self):
        PyELF.__init__(self)
        self.ei_class = bytearray(b'\x01') # Elf32 (Default, x86)
        self.ei_data = bytearray(b'\x01') # Little End (Default, x86)
        self.e_ident()
        self.e_mach = bytearray(b'\x03\x00') #dw
        self.ehdr()


    def hello2(self):
        self.baseaddr
        msg = bytearray(b'This is a Hello World compiled in Bryan\'s\
 experimental Python compiler!', 'ascii')
        msglen = bytearray([len(msg)+1])# \x0C = 12
        msgaddr = i_to_b(self.baseaddr+0x6e,8)
        self._binary = bytearray()
        self._binary += movAL + bytearray([4]) # syscall print a string
        self._binary += movDL + msglen # String Length
        self._binary += movECX + msgaddr # Pointer to start of string 
        self._binary += int80 # Go
        
        self._binary += movAL + bytearray([4]) # syscall print a string
        self._binary += movDL + msglen # String Length
        self._binary += movECX + msgaddr # Pointer to start of string 
        self._binary += int80 # Go
        
        self._binary += movAL + bytearray([1]) # syscall to exit
        self._binary += int80 # Go
        
        self._binary += msg + br
        return self._binary


if __name__ == '__main__':
    mybin = PyELF32_x86()
    hellobin = mybin.exe(mybin.hello())
    outpath = 'hello'
    myfile = open(outpath,'wb')
    myfile.write(hellobin)
    myfile.close()
