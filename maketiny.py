from opcodes import *
from pyelf import i_to_b

def makeprogramheader(baseaddr=None, filesize=None):
    if baseaddr == None: 
        baseaddr = 0x08081000
    p_vaddr = i_to_b(baseaddr,8)
    if filesize == None: 
        filesize = 0x0000006F
    p_filesz = i_to_b(filesize,8)
    p_memsz = p_filesz
    p_type = bytearray(b'\x01\x00\x00\x00') #dd
    p_offset = bytearray(b'\x00\x00\x00\x00') #dd
    p_paddr = p_vaddr #bytearray(b'\x00\x70\x04\x08') #dd
    p_flags = bytearray(b'\x05\x00\x00\x00') #dd
    p_align = bytearray(b'\x00\x10\x00\x00') #dd
    
    phdr = p_type \
        + p_offset \
        + p_vaddr \
        + p_paddr \
        + p_filesz \
        + p_memsz \
        + p_flags \
        + p_align
    return phdr
    
def makeelfheader(baseaddr=None):
    if baseaddr == None: 
        baseaddr = 0x08081000
    # These magic numbers say this is an ELF binary
    e_ident = bytearray(b'\x7F\x45\x4C\x46\x01\x01\x01\x00')
    # This is padding
    e_pad8 = bytearray(b'\x00\x00\x00\x00\x00\x00\x00\x00')
    # This specifies an x86 machine and the next two bytes 
    # are typically ignored
    e_type = bytearray(b'\x02\x00') #dw
    e_mach = bytearray(b'\x03\x00') #dw
    e_ver = bytearray(b'\x01\x00\x00\x00') #dd
    # Need to research this, some values of base address 
    # cause segfaults!  This specifies the absolute address
    # where the code starts
    e_entry = i_to_b(baseaddr+int(0x54),8)
    # Relative offset of the p[rogram] header
    e_phoff = bytearray(b'\x34\x00\x00\x00') #dd
    #e_phoff = i_to_b(0x34000000,8)
    # Relative header offset
    e_shoff = bytearray(b'\x00\x00\x00\x00') #dd
    e_flags = bytearray(b'\x00\x00\x00\x00') #dd 
    # ELF header size
    e_ehdrsize = bytearray(b'\x34\x00') #dw
    # Program header size
    e_phdrsize = bytearray(b'\x20\x00') #dw
    e_phnum = bytearray(b'\x01\x00') #dw
    e_shentsize = bytearray(b'\x00\x00') #dw
    e_shnum = bytearray(b'\x00\x00') #dw
    e_shstrndx = bytearray(b'\x00\x00') #dw
    
    ehdr = e_ident \
        + e_pad8 \
        + e_type \
        + e_mach \
        + e_ver \
        + e_entry \
        + e_phoff \
        + e_shoff \
        + e_flags \
        + e_ehdrsize \
        + e_phdrsize \
        + e_phnum \
        + e_shentsize \
        + e_shnum \
        + e_shstrndx
        
    return ehdr

def tinyelf(baseaddr):

    #Actual program starts here
    msg = bytearray(b'Hello World!', 'ascii')
    #msg = bytearray(b'\x48\x65\x6C\x6C\x6F\x20\x77\x6F\x72\x6C\x64')

    msglen = bytearray([len(msg)+1])# \x0C = 12
    #msgaddr = bytearray(b'\x63\x70\x04\x08')
    msgaddr = i_to_b(baseaddr+0x63,8)
    
    binary = bytearray()
    
    binary += movAL + bytearray([4]) # Linux System call to print a string
    binary += movDL + msglen # String Length
    binary += movECX + msgaddr # Pointer to start of string - see "msg" below

    binary += int80 # Go
    
    binary += movAL + bytearray([1]) # Linux system call to exit ?
    binary += int80 # Go
    
    binary += msg + br # Message itself tacked onto end of binary
    
    ehdr = makeelfheader(baseaddr=0x08081000)
    phdr = makeprogramheader(baseaddr=0x08081000,filesize=0x0000006F)
    
    # print (len(binary))
    binary = ehdr + phdr + binary
    hexstring = hex(len(binary)-len(msg)-1)
    # print(hexstring)
    # print(int(hexstring,0))
    
    return binary
    
if __name__ == '__main__':
    baseaddr = 0x08081000
    outpath = 'tiny'
    myfile = open(outpath,'wb')
    myfile.write(tinyelf(baseaddr))
    myfile.close()
    
    #print hex(int(msglen[0]))
    #p_vaddr.reverse()
    #print p_vaddr
    #base = "".join(["{:02x}".format(c) for c in p_vaddr])
    #print base
    #print hex(baseaddr + len(ehdr) + len(phdr))
